# Programmieraufgaben

## Programmieren Sie:

1. Einen rekursiven RMS Schätzer 
2. Eine Funktion zur A und C Gewichtung im Zeit- und Frequenzbereich
3. Eine Filterbank basieren auf dem IEC 1260 Standard mit Butterworth Filtern 6. Ordnung in SOS Struktur (ohne Abtastratenwandlung)
4. Ein vollständiges Terzband Analysesoftware mit der Möglichkeit ein Kalibriersignal anzugeben, um korrekte SPL Werte anzeigen zu können. Nutzen Sie alle Funktionen der AUfgaben 1-3.


## Zeigen Sie:

1. Dass das 4fache Antwortzeit den größten Teil der Impulsantwort umfasst, indem Sie zunächste die Hilberteinhüllende der Impulsantwort eines BAndpasses berechnen, die Antwortzeit bestimmen und das 4fache einzeichnen. (Aufgabe ist also Erzeugung einer Infografik)
2. Die Äquivalenz des rekursiven RMS Schätzers ,mit $\tau$ als Rekursionskonstante (Umrechnung in $\alpha$ notwendig) und dem Rechteckfenster mit der Fensterlänge $\tau$ (Umrechnung in Samples notwendig). Muss im Exponent -1,-2 oder -3 stehen?



