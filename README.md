# AMT_python

This repository contains all files for the lecture "Akustische Messtechnik" (Acoustic metrology):

Checkout with

git clone git@gitlab.gwdg.de:joerg.bitzer/amt_python.git --recursive

ToDo:

2) Files for third-octave IEC1260 signal analyse (FFT based filterbank)
3) Coherence as analysis tool


Done: 
1) Files for third-octave IEC1260 signal analyse (time-domain filterbank)

....

Es scheint immer Probleme zu geben unter Windows und GitLab. Eine Lösung war, die Schlüssel in .ssh zu löschen und neu zu generieren.
(Nur machen, wenn man bisher den Schlüssel nur für gitlab und github verwendet hat). Auf Gitlab den bisherigen Schlüssel löschen und den neuen public key hochladen. Bei der Generierung keine Passphrase eingeben (Dies umgeht einen Fehler bei GitLab beim auschecken mit submodulen.)

ssh-keygen -o -t rsa -b 4096 -C "vornmame.nachname@student.jade-hs.de"

nun sollte es gehen.

